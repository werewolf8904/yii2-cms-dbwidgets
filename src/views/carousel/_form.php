<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\composite\base\CompositeForm $model
 * @var yii\bootstrap\ActiveForm $form
 * @var \werewolf8904\cmscore\models\Language[] $languages
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div id="color-dialog" role="dialog" class="modal my-modal fade popup" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <output id="ajax_content"></output>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('backend', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="widget-carousel-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model->modelCarousel, 'key')->textInput(['maxlength' => 1024]) ?>
    <?= $form->field($model->modelCarousel, 'status')->checkbox(); ?>
    <hr>
    <?php
    $items = [];
    foreach ($languages as $language) {
        $items[] = [
            'name' => Yii::$app->language === $language->code ? 'image' : 'image_' . $language->code,
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Image') . ' ' . $language->code,
            'type' => \alexantr\elfinder\InputFile::class,
            'options' => [
                'clientRoute' => '//file/manager/input',
                'options' => [
                    'class' => 'form-control yii2-elfinder-input input_preview',
                    'data' => [
                        'default-src' => '/no_image.png',
                        'storage-src' => Yii::getAlias('@storageUrl/source')
                    ],
                ],
            ],
        ];
        $items[] = [
            'name' => Yii::$app->language === $language->code ? 'alt' : 'alt_' . $language->code,
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
            'enableError' => true,
        ];
        $items[] = [
            'name' => Yii::$app->language === $language->code ? 'content' : 'content_' . $language->code,
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
            'enableError' => true,
        ];
        $items[] = [
            'name' => Yii::$app->language === $language->code ? 'url' : 'url_' . $language->code,
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
            'enableError' => true,
        ];
        $items[] = [
            'name' => Yii::$app->language === $language->code ? 'title' : 'title_' . $language->code,
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Title') . ' ' . $language->code,
        ];
    }
    $columns = [
        [
            'name' => 'id',
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
            'enableError' => true,
        ],
        [
            'name' => 'id',
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_STATIC,
            'value' => function ($value) {
                if ($value && $value->id) {
                    $template = '<a href="' . \yii\helpers\Url::toRoute(['carousel-item/update', 'id' => $value->id,]) . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    //$template .= '<span></span><a href="' . \yii\helpers\Url::toRoute(['carousel-item/update', 'id' => $value->id,]) . '" class="ajax"><span class="glyphicon glyphicon-edit"></span></a>';
                    return $template;
                }
                return '';
            },
        ],
        [
            'name' => 'status',
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Status'),
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_CHECKBOX,
        ]
    ];
    $columns = array_merge($columns, $items);
    echo $form->field($model, 'modelsItem')->widget(\unclead\multipleinput\MultipleInput::class, [
        'min' => 0,
        'sortable' => true,
        'columns' => $columns,
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->modelCarousel->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->modelCarousel->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
