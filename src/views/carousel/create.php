<?php
/**
 * @var yii\web\View $this
 * @var werewolf8904\cmsdbwidgets\models\base\Carousel $model
 * @var \werewolf8904\cmscore\models\Language[] $languages
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Widget Carousel'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Carousels'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-carousel-create">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
