<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\composite\base\CompositeForm $model
 * @var \werewolf8904\cmscore\models\Language[] $languages
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Widget Carousel'),
    ]) . ' ' . $model->modelCarousel->key;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Carousels'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->modelCarousel->key,];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="widget-carousel-update">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
