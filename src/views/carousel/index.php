<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\search\CarouselSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\grid\GridView;
use yii\grid\SerialColumn;

$columns = [
    ['class' => SerialColumn::class],
    'id',
    'key',
    [
        'class' => EditableColumn::class,
        'attribute' => 'status',
        'filter' => [
            Yii::t('backend', 'Not Published'),
            Yii::t('backend', 'Published'),
        ],
        'vAlign' => 'middle',
        'editableOptions' => [
            'format' => Editable::FORMAT_BUTTON,
            'displayValueConfig' => ['0' => GridView::ICON_INACTIVE, '1' => GridView::ICON_ACTIVE],
            'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
            'placement' => 'auto',
        ],
    ],
]; ?>
<?= $this->render('@core/views/_common/index', [
    'columns' => $columns,
    'title' => Yii::t('backend', 'Widget Carousels'),
    'title_create' => Yii::t('backend', 'Widget Carousel'),
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel
]) ?>