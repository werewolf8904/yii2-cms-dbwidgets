<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\CarouselItem $model
 * @var yii\bootstrap\ActiveForm $form
 * */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="widget-carousel-item-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>
    <?= $form->field($model, 'status')->checkbox(); ?>
    <?= $form->field($model, 'sort')->textInput(); ?>
    <hr>
    <?= \yii\bootstrap\Tabs::widget([
        'items' => \yii\helpers\ArrayHelper::getColumn(
            $languages,
            function ($language) use ($form, $model) {
                /**
                 * @var \werewolf8904\cmscore\models\Language $language
                 */
                return [
                    'label' => $language->name,
                    'content' => $this->render('_form_i18n', compact('form', 'model', 'language'))
                ];
            },
            false
        ),
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
