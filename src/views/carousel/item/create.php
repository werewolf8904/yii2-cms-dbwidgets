<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\CarouselItem $model
 * @var werewolf8904\cmsdbwidgets\models\base\Carousel $carousel
 * @var \werewolf8904\cmscore\models\Language[] $languages
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Widget Carousel Item')
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Carousel Items'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $carousel->key, 'url' => ['update', 'id' => $carousel->id,],];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Create');
?>
<div class="widget-carousel-item-create">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
