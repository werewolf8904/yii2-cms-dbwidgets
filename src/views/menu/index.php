<?php
/**
 * @var $this yii\web\View
 * @var $searchModel \werewolf8904\cmsdbwidgets\models\backend\search\MenuSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\grid\GridView;
use yii\grid\SerialColumn;

$columns = [
    ['class' => SerialColumn::class,],
    'key',
    [
        'class' => EditableColumn::class,
        'attribute' => 'status',
        'filter' => [
            Yii::t('backend', 'Not Published'),
            Yii::t('backend', 'Published'),
        ],
        'vAlign' => 'middle',
        'editableOptions' => [
            'format' => Editable::FORMAT_BUTTON,
            'displayValueConfig' => ['0' => GridView::ICON_INACTIVE, '1' => GridView::ICON_ACTIVE,],
            'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
            'placement' => 'auto',
        ],
    ],
]; ?>
<?= $this->render('@core/views/_common/index', [
    'columns' => $columns,
    'title' => Yii::t('backend', 'Widget Menus'),
    'title_create' => Yii::t('backend', 'Widget Menu'),
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
]) ?>

