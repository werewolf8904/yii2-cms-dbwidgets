<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\composite\base\CompositeForm
 * @var $form yii\bootstrap\ActiveForm
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

use werewolf8904\cmsdbwidgets\links\IUrlData;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div id="color-dialog" role="dialog" class="modal my-modal fade popup" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <output id="ajax_content"></output>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('backend', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="widget-carousel-form">
    <?php
    $data = Yii::$container->get(IUrlData::class)->getDataArray();
    ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model->modelMenu, 'key')->textInput(['maxlength' => 1024,]) ?>
    <?= $form->field($model->modelMenu, 'status')->checkbox(); ?>
    <hr>
    <?php
    $items = [];
    $columns = [
        [
            'name' => 'id',
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
            'enableError' => true,
        ],
        [
            'title' => Yii::t('backend', 'Url'),
            'name' => 'urlData',
            'value' => 'urlData',
            'type' => \kartik\widgets\Select2::class,
            'options' => [
                'data' => $data,
                'options' => [
                    'placeholder' => '',
                    'prompt' => '-',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'language' => [
                        'errorLoading' => new \yii\web\JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                ],
            ],
        ],
        [
            'name' => 'status',
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Status'),
            'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_CHECKBOX,
        ],
    ];
    foreach (Yii::$container->get(\werewolf8904\cmscore\language\ILanguage::class)->getLanguages() as $key => $value) {
        $columns[] = [
            'name' => 'name_' . $key,
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Name') . ' ' . $key,
        ];
        $columns[] = [
            'name' => 'url_' . $key,
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Url') . ' ' . $key,
        ];
    }
    $columns = array_merge($columns, $items);
    echo $form->field($model, 'modelsItem')->widget(\unclead\multipleinput\MultipleInput::class, [
        'min' => 0,
        'sortable' => true,
        'columns' => $columns,
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->modelMenu->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->modelMenu->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
