<?php
/**
 * @var $this yii\web\View
 * @var $model werewolf8904\cmsdbwidgets\models\base\Carousel
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Widget Carousel'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Carousels'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-carousel-create">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
