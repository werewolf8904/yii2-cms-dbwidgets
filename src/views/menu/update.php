<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\composite\base\CompositeForm
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Widget Menu'),
    ]) . ' ' . $model->modelMenu->key;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Menus'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->modelMenu->key,];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="widget-carousel-update">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
