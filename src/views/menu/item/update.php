<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmsdbwidgets\models\backend\CarouselItem
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Widget Carousel Item'),
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Carousel Items'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->carousel->key, 'url' => ['widget-carousel/update', 'id' => $model->carousel->id,],];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="widget-carousel-item-update">
    <?php \yii\widgets\Pjax::begin(['enablePushState' => false, 'id' => $model->id,]) ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>
