<?php
/**
 * @var $this  yii\web\View
 * @var $model \werewolf8904\cmsdbwidgets\models\backend\CarouselItem
 * @var $form  yii\bootstrap\ActiveForm
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="widget-carousel-item-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>
    <?= $form->field($model, 'image')->widget(\alexantr\elfinder\InputFile::class, [
        'clientRoute' => '//file/manager/input',
        'options' => [
            'class' => 'form-control yii2-elfinder-input input_preview',
            'data' => [
                'default-src' => '/no_image.png',
                'storage-src' => Yii::getAlias('@storageUrl/source'),
            ],
        ],
    ]); ?>
    <?= $form->field($model, 'order')->textInput(); ?>
    <?= $form->field($model, 'status')->checkbox(); ?>
    <hr>
    <?= \yii\bootstrap\Tabs::widget([
        'items' => \yii\helpers\ArrayHelper::getColumn(
            $languages,
            function ($language) use ($form, $model) {
                /**
                 * @var \werewolf8904\cmscore\models\Language $language
                 */
                return [
                    'label' => $language->name,
                    'content' => $this->render('_form_i18n', compact('form', 'model', 'language'))
                ];
            },
            false
        ),
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
