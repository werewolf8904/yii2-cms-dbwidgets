<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\CarouselItem $model
 * @var yii\bootstrap\ActiveForm $form
 * @var \werewolf8904\cmscore\models\Language $language
 */

use alexantr\ckeditor\CKEditor;

echo $form->field(
    $model,
    $model->defaultLanguage == $language->code ? 'content' : 'content_' . $language->code
)->label($model->getAttributeLabel('content'))->widget(CKEditor::class);
echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'title' : 'title_' . $language->code
)->label($model->getAttributeLabel('title'));
echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'alt' : 'alt_' . $language->code
)->label($model->getAttributeLabel('alt'));
echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'url' : 'url_' . $language->code
)->label($model->getAttributeLabel('url'))->textInput();
