<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\Image $model
 * @var yii\bootstrap\ActiveForm $form
 * @var \werewolf8904\cmscore\models\Language $language
 */

echo $form->field($model, $model->defaultLanguage === $language->code ? 'image' : 'image_' . $language->code)->widget(\alexantr\elfinder\InputFile::class, [
    'clientRoute' => '//file/manager/input',
    'options' => [
        'class' => 'form-control yii2-elfinder-input input_preview',
        'data' => [
            'default-src' => '/no_image.png',
            'storage-src' => Yii::getAlias('@storageUrl/source'),
        ],
    ],
])->label($model->getAttributeLabel('image'));
echo $form->field($model, $model->defaultLanguage === $language->code ? 'image_media' : 'image_media_' . $language->code)
    ->widget(\unclead\multipleinput\MultipleInput::class, [
        'min' => 0,
        'sortable' => true,
        'columns' => [
            [
                'name' => 'image',
                'type' => \alexantr\elfinder\InputFile::class,
                'options' => [
                    'clientRoute' => '//file/manager/input',
                    'options' => [
                        'class' => ['form-control', 'yii2-elfinder-input', 'input_preview',],
                        'data' => [
                            'default-src' => '/no_image.png',
                            'storage-src' => Yii::getAlias('@storageUrl/source'),
                        ],
                    ],
                ],
            ],
            [
                'name' => 'media',
                'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_DROPDOWN,
                'items' => \werewolf8904\cmsdbwidgets\helpers\Media::getList(),
            ],
        ],
    ])->label($model->getAttributeLabel('image_media'));
echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'title' : 'title_' . $language->code
)->label($model->getAttributeLabel('title'));
echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'alt' : 'alt_' . $language->code
)->label($model->getAttributeLabel('alt'));

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'url' : 'url_' . $language->code
)->label($model->getAttributeLabel('url'))->textInput();
