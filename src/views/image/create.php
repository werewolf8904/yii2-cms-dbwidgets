<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\Image $model
 * @var \werewolf8904\cmscore\models\Language[] $languages
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Widget Image'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Images'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-image-create">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
