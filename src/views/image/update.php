<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\Image $model
 * @var \werewolf8904\cmscore\models\Language[] $languages
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Widget Image'),
    ]) . ' ' . $model->key;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Widget Images'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->key,];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="widget-image-update">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
