<?php
/**
 * @var yii\web\View $this
 * @var \werewolf8904\cmsdbwidgets\models\backend\Image $model
 * @var yii\bootstrap\ActiveForm $form
 * @var \werewolf8904\cmscore\models\Language[] $languages
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;

?>
<div class="widget-image-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'key')->textInput(['maxlength' => true,]) ?>
    <?= $form->field($model, 'status')->checkbox(); ?>
    <hr>
    <?= Tabs::widget([
        'items' => \yii\helpers\ArrayHelper::getColumn(
            $languages,
            function ($language) use ($form, $model) {
                /**
                 * @var \werewolf8904\cmscore\models\Language $language
                 */
                return [
                    'label' => $language->name,
                    'content' => $this->render('_form_i18n', compact('form', 'model', 'language'))
                ];
            },
            false
        )
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
