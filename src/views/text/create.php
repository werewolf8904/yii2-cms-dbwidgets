<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmsdbwidgets\models\backend\Text
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => Yii::t('backend', 'Text Block'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Text Blocks'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-block-create">
    <?= $this->render('_form', compact('model', 'languages')); ?>
</div>
