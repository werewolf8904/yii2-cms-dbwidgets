<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmsdbwidgets\models\backend\Text
 * @var $languages \werewolf8904\cmscore\models\Language[]
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('backend', 'Text Block'),
    ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Text Blocks'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->name,];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="text-block-update">
    <?= $this->render('_form', compact('model', 'languages')) ?>
</div>
