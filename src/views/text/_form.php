<?php
/**
 * @var $this      yii\web\View
 * @var $model     \werewolf8904\cmsdbwidgets\models\backend\Text
 * @var $form      yii\bootstrap\ActiveForm
 * @var $languages \werewolf8904\cmscore\models\Language[]
 * */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="article-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'key')->textInput(['maxlength' => 1024,]) ?>
    <?= $form->field($model, 'status')->checkbox(); ?>
    <hr>
    <?= \yii\bootstrap\Tabs::widget([
        'items' => \yii\helpers\ArrayHelper::getColumn(
            $languages,
            function ($language) use ($form, $model) {
                /**
                 * @var \werewolf8904\cmscore\models\Language $language
                 */
                return [
                    'label' => $language->name,
                    'content' => $this->render('_form_i18n', compact('form', 'model', 'language'))
                ];
            },
            false
        ),
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]
        ) ?>
        <?= Html::submitButton(Yii::t('backend', 'Save and stay'), ['class' => 'btn btn-success', 'name' => 'stay',]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
