<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmsdbwidgets\models\backend\Text
 * @var $form yii\bootstrap\ActiveForm
 * @var $language \werewolf8904\cmscore\models\Language
 */

use alexantr\ckeditor\CKEditor;

echo $form->field(
    $model,
    $model->defaultLanguage === $language->code ? 'name' : 'name_' . $language->code
)->label($model->getAttributeLabel('name'))->textInput();
echo $form->field(
    $model,
    $model->defaultLanguage == $language->code ? 'content' : 'content_' . $language->code
)->label($model->getAttributeLabel('content'))->widget(CKEditor::class);
