<?php

namespace werewolf8904\cmsdbwidgets\widgets;

use werewolf8904\cmsdbwidgets\models\frontend\Text;
use Yii;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;

/**
 * Class DbText
 * Return a text block content stored in db
 *
 */
class DbText extends Widget
{
    /**
     * @var string text block key
     */
    public $key;

    /**
     * @var string
     */
    public $view_item = 'text';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_item,
                    $this->key,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => [
                        \werewolf8904\cmsdbwidgets\models\base\Text::class,
                        \werewolf8904\cmsdbwidgets\models\base\Text::class . $this->key,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        $output = YII_ENV_DEV ? "<!-- Widget text block '{$this->key}' -->" : '';
        $model = Text::find()->andWhere(['key' => $this->key, 'status' => Text::STATUS_ACTIVE,])->one();
        if ($model) {
            if ($this->view_item) {
                return $output . $this->render($this->view_item, compact('model'));
            }
            return $output . $model->content;
        }
        return $output;
    }
}
