<?php

namespace werewolf8904\cmsdbwidgets\widgets;

use werewolf8904\cmsdbwidgets\models\base\Menu;
use Yii;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;

/**
 * Class DbMenuWidget
 *
 */
class DbMenuWidget extends Widget
{
    /**
     * @var string
     */
    public $view_item;

    /**
     * @var string
     */
    public $key;

    /**
     * @var
     */
    public $model;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_item,
                    $this->key,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => [
                        Menu::class,
                        Menu::class . $this->key,
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $output = YII_ENV_DEV ? "<!-- Widget menu '{$this->key}' -->" : '';
        $menu = Menu::find()->andWhere(['key' => $this->key,])->one();
        /**
         * @var $menu Menu
         */
        $items = $menu ? $menu->getMenuItems()->andWhere(['status' => 1,])->asArray()->all() : [];
        if (\count($items) < 1) {
            return $output;
        }
        $menu_items = [];
        foreach ($items as $item) {
            /** @var $model \werewolf8904\cmsdbwidgets\links\MenuItemInterface */
            $model = Yii::createObject($item['menu_class'], [$item['data'], Yii::$app->language,]);
            $menu_items[] = [
                'url' => $model->getUrl(),
                'name' => $model->getName(),
            ];
        }
        return $output . $this->render($this->view_item, compact('menu_items'));
    }
}
