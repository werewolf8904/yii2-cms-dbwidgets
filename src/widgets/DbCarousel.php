<?php

namespace werewolf8904\cmsdbwidgets\widgets;

use werewolf8904\cmsdbwidgets\models\base\query\CarouselItemQuery;
use werewolf8904\cmsdbwidgets\models\frontend\Carousel;
use werewolf8904\cmsdbwidgets\models\frontend\CarouselItem;
use Yii;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;
use yii\helpers\Html;

/**
 * Class DbCarousel
 *
 */
class DbCarousel extends Widget
{
    /** @var string */
    public $key;

    /** @var string */
    public $view_wrap;

    /** @var string */
    public $view_item;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_wrap,
                    $this->view_item,
                    $this->key,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => [
                        \werewolf8904\cmsdbwidgets\models\base\Carousel::class,
                        \werewolf8904\cmsdbwidgets\models\base\Carousel::class . $this->key,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $output = YII_ENV_DEV ? "<!-- Widget carousel '{$this->key}' -->" : '';
        $carousel = Carousel::find()
            ->joinWith(['items' => function (CarouselItemQuery $query) {
                $query->published()->addOrderBy(['sort' => SORT_DESC,]);
            },])
            ->published()
            ->andWhere([
                Carousel::tableName() . '.key' => $this->key,
            ])
            ->orderBy(['sort' => SORT_DESC,])
            ->one();
        if ($carousel) {
            $carousel_items = [];
            if ($this->view_item === null && $this->view_wrap !== null) {
                return $output . $this->render($this->view_wrap, ['carousel' => $carousel, 'carousel_items' => $carousel->items,]);
            }
            foreach ($carousel->items as $index => $carousel_item) {
                /** @var CarouselItem $carousel_item */
                $carousel_items[$index] = isset($this->view_item)
                    ? $this->render($this->view_item, ['carousel_item' => $carousel_item, 'index' => $index,])
                    : Html::a(
                        Html::img(
                            $carousel_item->getImageUrl(),
                            ['alt' => $carousel_item->alt,]
                        ),
                        $carousel_item->url,
                        ['title' => $carousel_item->title,]
                    );
            }
            return $output . (
                isset($this->view_wrap)
                    ? $this->render($this->view_wrap, compact('carousel', 'carousel_items'))
                    : implode(PHP_EOL, $carousel_items)
                );
        }
        return $output;
    }
}
