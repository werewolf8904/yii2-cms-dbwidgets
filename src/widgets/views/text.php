<?php
/**
 * @var $model \werewolf8904\cmsdbwidgets\models\frontend\Text
 */
?>
<h2><?= $model->name ?></h2>
<div>
    <?= $model->content ?>
</div>
