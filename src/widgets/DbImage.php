<?php

namespace werewolf8904\cmsdbwidgets\widgets;

use werewolf8904\cmsdbwidgets\models\frontend\Image;
use Yii;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;

/**
 * Class DbImage
 *
 */
class DbImage extends Widget
{
    /**
     * @var string text block key
     */
    public $key;

    /**
     * @var
     */
    public $view_item;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_item,
                    $this->key,
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => [
                        \werewolf8904\cmsdbwidgets\models\base\Image::class,
                        \werewolf8904\cmsdbwidgets\models\base\Image::class . $this->key,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        $output = YII_ENV_DEV && $this->view_item ? "<!-- Widget image '{$this->key}' -->" : '';
        $model = Image::find()->andWhere(['key' => $this->key, 'status' => Image::STATUS_ACTIVE,])->one();
        if ($model) {
            if ($this->view_item) {
                return $output . $this->render($this->view_item, compact('model'));
            }
            return \yii\helpers\ArrayHelper::getValue($model, ['image',], $output);
        }
        return $output;
    }
}
