<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace werewolf8904\cmsdbwidgets\helpers;

/**
 * Class Media
 * @package werewolf8904\cmsdbwidgets\helpers
 */
class Media
{
    /**
     * @return array
     */
    public static function getList()
    {
        return [
            '1366' => '1366',
            '1199' => '1199',
            '1024' => '1024',
            '991' => '991',
            '767' => '767',
            '460' => '460',
            '375' => '375',
        ];
    }
}
