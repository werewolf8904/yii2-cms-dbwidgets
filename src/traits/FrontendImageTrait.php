<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace werewolf8904\cmsdbwidgets\traits;

use yii\helpers\ArrayHelper;

/**
 * Trait FrontendImageTrait
 * @package werewolf8904\cmsdbwidgets\traits
 */
trait FrontendImageTrait
{
    /**
     * @var
     */
    public $image;

    /**
     * @var
     */
    public $alt;

    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    public $url;

    /**
     * @var
     */
    public $image_media;


    /**
     * @inheritDoc
     */
    public function afterFind()
    {
        parent::afterFind();
        try {
            $this->image_media = json_decode($this->image_media, true);
        } catch (\Exception $exception) {
            $this->image_media = [];
        }
    }

    /**
     * @param string $preset
     * @param array $params
     * @param null $media
     * @return string
     */
    public function getImageUrl($preset = null, $params = [], $media = null): string
    {
        return \Yii::$container
            ->get(\code2magic\glide\components\IGlide::class)
            ->getGlideThumbnail($this->getImageByMedia($media), $preset, $params);
    }

    /**
     * @param null $media
     * @return mixed
     */
    public function getImageByMedia($media = null){
        if ($media){
            foreach ($this->image_media as $image_media_item) {
                if (($output = ArrayHelper::getValue($image_media_item, ['media'])) !== $media){
                    return $output;
                }
            }
        }
        return $this->image;
    }
}
