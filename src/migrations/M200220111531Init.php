<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace werewolf8904\cmsdbwidgets\migrations;

use yii\helpers\Console;

/**
 * Class M200220111531Init
 */
class M200220111531Init extends \werewolf8904\cmscore\db\Migration
{
    /**
     * @inheritDoc
     */
    public function safeUp()
    {
        // widget_text
        if (!$this->checkIsTableExists('{{%widget_text}}', 'exists')) {
            $this->createTable('{{%widget_text}}', [
                'id' => $this->primaryKey(),
                'key' => $this->string()->notNull(),
                'status' => $this->smallInteger()->notNull()->defaultValue(0),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ], $this->tableOptions);
            $this->createIndex('idx-widget_text-key', '{{%widget_text}}', 'key', true);
        }
        // widget_text_i18n
        if (!$this->checkIsTableExists('{{%widget_text_i18n}}', 'exists')) {
            $this->createTable('{{%widget_text_i18n}}', [
                'id' => $this->primaryKey(),
                'language_code' => $this->string(16)->notNull()->append('COLLATE "utf8_general_ci"'),
                'widget_text_id' => $this->integer()->notNull(),
                'name' => $this->string()->notNull()->defaultValue(''),
                'content' => $this->text(),
            ], $this->tableOptions);
            $this->createIndex('idx-widget_text-i18n--id-language_code', '{{%widget_text_i18n}}', ['widget_text_id', 'language_code',], true);
            $this->addForeignKey(
                'fk-widget-text_i18n-2-text',
                '{{%widget_text_i18n}}', 'widget_text_id',
                '{{%widget_text}}', 'id',
                'CASCADE', 'CASCADE'
            );
            $this->addForeignKey(
                'fk-widget_text_i18n-2-language',
                '{{%widget_text_i18n}}', 'language_code',
                '{{%language}}', 'code',
                'CASCADE', 'CASCADE'
            );
        }
        // widget_carousel
        if (!$this->checkIsTableExists('{{%widget_carousel}}', 'exists')) {
            $this->createTable('{{%widget_carousel}}', [
                'id' => $this->primaryKey(),
                'key' => $this->string(255)->notNull(),
                'status' => $this->tinyInteger(1)->notNull()->defaultValue(0)
            ], $this->tableOptions);
            $this->createIndex('idx-widget_carousel-key', '{{%widget_carousel}}', 'key', true);
        }
        // widget_carousel_item
        if (!$this->checkIsTableExists('{{%widget_carousel_item}}', 'exists')) {
            $this->createTable('{{%widget_carousel_item}}', [
                'id' => $this->primaryKey(),
                'carousel_id' => $this->integer()->notNull(),
                'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
                'sort' => $this->integer()->notNull()->defaultValue(0),
                'data' => $this->text(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ], $this->tableOptions);
            $this->addForeignKey(
                'fk-widget-carousel_item-2-carousel',
                '{{%widget_carousel_item}}', 'carousel_id',
                '{{%widget_carousel}}', 'id',
                'CASCADE', 'CASCADE'
            );
        }
        // widget_carousel_item_i18n
        if (!$this->checkIsTableExists('{{%widget_carousel_item_i18n}}', 'exists')) {
            $this->createTable('{{%widget_carousel_item_i18n}}', [
                'id' => $this->primaryKey(),
                'language_code' => $this->string(16)->notNull()->append('COLLATE "utf8_general_ci"'),
                'widget_carousel_item_id' => $this->integer()->notNull(),
                'image' => $this->string(1024)->notNull()->defaultValue(''),
                'image_media' => $this->json(),
                'content' => $this->text(),
                'title' => $this->string()->notNull()->defaultValue(''),
                'alt' => $this->string()->notNull()->defaultValue(''),
                'url' => $this->string(1024)->notNull()->defaultValue(''),
            ], $this->tableOptions);
            $this->createIndex(
                'idx-widget_carousel_item-i18n--id-language_code',
                '{{%widget_carousel_item_i18n}}',
                ['widget_carousel_item_id', 'language_code',],
                true
            );
            $this->addForeignKey(
                'fk-widget_carousel-item_i18n-2-item',
                '{{%widget_carousel_item_i18n}}', 'widget_carousel_item_id',
                '{{%widget_carousel_item}}', 'id',
                'CASCADE', 'CASCADE'
            );
            $this->addForeignKey(
                'fk-widget_carousel_item_i18n-2-language',
                '{{%widget_carousel_item_i18n}}', 'language_code',
                '{{%language}}', 'code',
                'CASCADE', 'CASCADE'
            );
        }
        // widget_image
        if (!$this->checkIsTableExists('{{%widget_image}}', 'exists')) {
            $this->createTable('{{%widget_image}}', [
                'id' => $this->primaryKey(),
                'key' => $this->string()->notNull(),
                'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ], $this->tableOptions);
            $this->createIndex('idx-widget_image-key', '{{%widget_image}}', 'key', true);
        }
        // widget_image_i18n
        if (!$this->checkIsTableExists('{{%widget_image_i18n}}', 'exists')) {
            $this->createTable('{{%widget_image_i18n}}', [
                'id' => $this->primaryKey(),
                'language_code' => $this->string(16)->notNull()->append('COLLATE "utf8_general_ci"'),
                'widget_image_id' => $this->integer()->notNull(),
                'image' => $this->string(1024)->notNull()->defaultValue(''),
                'image_media' => $this->json(),
                'url' => $this->string(1024)->notNull()->defaultValue(''),
                'alt' => $this->string()->notNull()->defaultValue(''),
                'title' => $this->string()->notNull()->defaultValue(''),
            ], $this->tableOptions);
            $this->createIndex(
                'idx-widget_image-i18n--id-language_code',
                '{{%widget_image_i18n}}',
                ['widget_image_id', 'language_code',],
                true
            );
            $this->addForeignKey(
                'fk-widget-image_i18n-2-image',
                '{{%widget_image_i18n}}', 'widget_image_id',
                '{{%widget_image}}', 'id',
                'CASCADE', 'CASCADE'
            );
            $this->addForeignKey(
                'fk-widget_image_i18n-2-language',
                '{{%widget_image_i18n}}', 'language_code',
                '{{%language}}', 'code',
                'CASCADE', 'CASCADE'
            );
        }
        // widget_menu
        if (!$this->checkIsTableExists('{{%widget_menu}}', 'exists')) {
            $this->createTable('{{%widget_menu}}', [
                'id' => $this->primaryKey(),
                'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
                'key' => $this->string(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $this->tableOptions);
            $this->createIndex('idx-widget_menu-key', '{{%widget_menu}}', 'key', true);
        }
        // widget_menu_item
        if (!$this->checkIsTableExists('{{%widget_menu_item}}', 'exists')) {
            $this->createTable('{{%widget_menu_item}}', [
                'id' => $this->primaryKey(),
                'menu_id' => $this->integer()->notNull(),
                'status' => $this->tinyInteger(1)->notNull()->defaultValue(0),
                'sort' => $this->tinyInteger(1)->notNull()->defaultValue(0),
                'menu_class' => $this->string()->notNull()->defaultValue(''),
                'data' => $this->text(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $this->tableOptions);
            $this->addForeignKey(
                'fk-widget-menu_item-2-menu',
                '{{%widget_menu_item}}', 'menu_id',
                '{{%widget_menu}}', 'id',
                'CASCADE', 'CASCADE'
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function safeDown()
    {
        // widget_menu_item
        if ($this->checkIsTableExists('{{%widget_menu_item}}', 'not exists')) {
            $this->dropForeignKey(
                'fk-widget-menu_item-2-menu',
                '{{%widget_menu_item}}'
            );
            $this->dropTable('{{%widget_menu_item}}');
        }
        // widget_menu
        if ($this->checkIsTableExists('{{%widget_menu}}', 'not exists')) {
            $this->dropIndex('idx-widget_menu-key', '{{%widget_menu}}');
            $this->dropTable('{{%widget_menu}}');
        }
        // widget_image_i18n
        if ($this->checkIsTableExists('{{%widget_image_i18n}}', 'not exists')) {
            $this->dropForeignKey(
                'fk-widget_image_i18n-2-language',
                '{{%widget_image_i18n}}'
            );
            $this->dropForeignKey(
                'fk-widget-image_i18n-2-image',
                '{{%widget_image_i18n}}'
            );
            $this->dropIndex(
                'idx-widget_image-i18n--id-language_code',
                '{{%widget_image_i18n}}'
            );
            $this->dropTable('{{%widget_image_i18n}}');
        }
        // widget_image
        if ($this->checkIsTableExists('{{%widget_image}}', 'not exists')) {
            $this->dropIndex('idx-widget_image-key', '{{%widget_image}}');
            $this->dropTable('{{%widget_image}}');
        }
        // widget_carousel_item_i18n
        if ($this->checkIsTableExists('{{%widget_carousel_item_i18n}}', 'not exists')) {
            $this->dropForeignKey('fk-widget_carousel-item_i18n-2-item', '{{%widget_carousel_item_i18n}}');
            $this->dropForeignKey('fk-widget_carousel_item_i18n-2-language', '{{%widget_carousel_item_i18n}}');
            $this->dropIndex(
                'idx-widget_carousel_item-i18n--id-language_code',
                '{{%widget_carousel_item_i18n}}'
            );
            $this->dropTable('{{%widget_carousel_item_i18n}}');
        }
        // widget_carousel_item
        if ($this->checkIsTableExists('{{%widget_carousel_item}}', 'not exists')) {
            $this->dropForeignKey('fk-widget-carousel_item-2-carousel', '{{%widget_carousel_item}}');
            $this->dropTable('{{%widget_carousel_item}}');
        }
        // widget_carousel
        if ($this->checkIsTableExists('{{%widget_carousel}}', 'not exists')) {
            $this->dropIndex('idx-widget_carousel-key', '{{%widget_carousel}}');
            $this->dropTable('{{%widget_carousel}}');
        }
        // widget_text_i18n
        if ($this->checkIsTableExists('{{%widget_text_i18n}}', 'not exists')) {
            $this->dropForeignKey('fk-widget-text_i18n-2-text', '{{%widget_text_i18n}}');
            $this->dropForeignKey('fk-widget_text_i18n-2-language', '{{%widget_text_i18n}}');
            $this->dropIndex('idx-widget_text-i18n--id-language_code', '{{%widget_text_i18n}}');
            $this->dropTable('{{%widget_text_i18n}}');
        }
        // widget_text
        if ($this->checkIsTableExists('{{%widget_text}}', 'not exists')) {
            $this->dropIndex('idx-widget_text-key', '{{%widget_text}}');
            $this->dropTable('{{%widget_text}}');
        }
    }

    /**
     * @param string $tableName
     */
    protected function checkIsTableExists($tableName, $info_if = 'exists'){
        $output = $this->db->getTableSchema($tableName, true) !== null;
        if ($output && $info_if === 'exists') {
            Console::output(Console::ansiFormat("Table {$tableName} exists!", [Console::FG_YELLOW,]));
        } elseif(!$output && $info_if === 'not exists') {
            Console::output(Console::ansiFormat("Table {$tableName} doesn`t exists!", [Console::FG_YELLOW,]));
        }
        return $output;
    }
}
