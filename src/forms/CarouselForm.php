<?php

namespace werewolf8904\cmsdbwidgets\forms;

use werewolf8904\cmsdbwidgets\models\backend\Carousel;
use werewolf8904\cmsdbwidgets\models\backend\CarouselItem;
use werewolf8904\composite\base\CompositeForm;

/**
 * Class WidgetCarouselForm
 *
 * @property Carousel $modelCarousel
 * @property CarouselItem[] $modelsItem
 */
class CarouselForm extends CompositeForm
{
    /**
     * @inheritdoc
     */
    public $models = [
        'modelCarousel' => null,
        'modelsItem' => [],
    ];

    /**
     * @inheritdoc
     */
    public $modelsClass = [
        'modelCarousel' => Carousel::class,
        'modelsItem' => CarouselItem::class,
    ];

    /**
     * @inheritdoc
     */
    public $not_copy_parameters = [
        'key',
        'id',
    ];

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'modelCarousel' => \Yii::t('cmsdbwidgets/model_labels', 'Carousel'),
            'modelsItem' => \Yii::t('cmsdbwidgets/model_labels', 'Carousel items'),
        ];
    }
}
