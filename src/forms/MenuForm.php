<?php


namespace werewolf8904\cmsdbwidgets\forms;


use werewolf8904\cmsdbwidgets\models\base\Menu;
use werewolf8904\cmsdbwidgets\models\base\MenuItem;
use werewolf8904\composite\base\CompositeForm;

/**
 * Class WidgetCarouselForm
 *
 * @property Menu $modelMenu
 * @property MenuItem[] $modelsItem
 */
class MenuForm extends CompositeForm
{
    /**
     * @var array
     */
    public $models = [
        'modelMenu' => null,
        'modelsItem' => [],
    ];

    /**
     * @var array
     */
    public $modelsClass = [
        'modelMenu' => Menu::class,
        'modelsItem' => MenuItem::class,
    ];

    /**
     * @var array
     */
    public $not_copy_parameters = [
        'key',
    ];

    /**
     * @var bool
     */
    public $needInitialize = false;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['modelMenu'] = \Yii::t('cmsdbwidgets/model_labels', 'Menu');
        $labels['modelsItem'] = \Yii::t('cmsdbwidgets/model_labels', 'Menu items');
        return $labels;
    }
}
