<?php

namespace werewolf8904\cmsdbwidgets\links;


interface MenuItemInterface
{
    public function getUrl();

    public function getName();
}