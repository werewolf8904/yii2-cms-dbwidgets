<?php

namespace werewolf8904\cmsdbwidgets\links;

interface MenuDataConverterInterface
{
    public function decodeData($data);

    public function encodeData($data);
}