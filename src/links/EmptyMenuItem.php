<?php

namespace werewolf8904\cmsdbwidgets\links;


class EmptyMenuItem implements MenuItemInterface
{


    public function getUrl()
    {
        return '#';
    }

    public function getName()
    {
        return '';
    }
}