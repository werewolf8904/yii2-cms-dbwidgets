<?php

namespace werewolf8904\cmsdbwidgets\links;

use Yii;
use yii\helpers\ArrayHelper;

class LinkFactory
{

    protected $_converter;

    public function __construct(MenuDataConverterInterface $converter)
    {
        $this->_converter = $converter;
    }

    public function createFromEncoded($data, $lang): MenuItemInterface
    {
        $data = $this->_converter->decodeData($data);
        try {
            return (MenuItemInterface::class)(Yii::createObject(ArrayHelper::getValue($data, 'class'), [ArrayHelper::getValue($data, 'data'), $lang]));
        } catch (\Exception $e) {
            return new EmptyMenuItem();
        }
    }
}