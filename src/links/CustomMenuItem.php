<?php


namespace werewolf8904\cmsdbwidgets\links;


use yii\helpers\ArrayHelper;

class CustomMenuItem implements MenuItemInterface
{
    protected $_data;
    protected $_lang;
    protected $_model;

    public function __construct($data, $lang)
    {
        $this->_data = json_decode(ArrayHelper::getValue(json_decode($data, true), 'data'), true);
        $this->_lang = $lang;
    }

    public function getUrl()
    {
        return ArrayHelper::getValue($this->_data, 'url_' . $this->_lang);
    }

    public function getName()
    {
        return ArrayHelper::getValue($this->_data, 'name_' . $this->_lang);
    }
}