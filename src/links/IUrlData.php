<?php


namespace werewolf8904\cmsdbwidgets\links;


interface IUrlData
{

    public function getDataArray(): array;
}