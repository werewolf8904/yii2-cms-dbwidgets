<?php

namespace werewolf8904\cmsdbwidgets\links;


class MenuDataConverter implements MenuDataConverterInterface
{

    public function decodeData($data)
    {
        return json_decode(base64_decode($data));
    }

    public function encodeData($data)
    {
        return base64_encode(json_encode($data));

    }
}