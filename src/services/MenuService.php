<?php

namespace werewolf8904\cmsdbwidgets\services;


use werewolf8904\cmsdbwidgets\forms\MenuForm;
use werewolf8904\cmsdbwidgets\models\base\Menu;
use werewolf8904\composite\service\BaseService;

class MenuService extends BaseService
{

    public $model_class = MenuForm::class;
    public $main_model = 'modelMenu';
    public $main_model_class = Menu::class;
    public $relations = [
        'modelsItem' => 'menuItems'

    ];


}