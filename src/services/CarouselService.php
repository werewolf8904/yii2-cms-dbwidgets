<?php
namespace werewolf8904\cmsdbwidgets\services;

use werewolf8904\composite\service\BaseService;
use werewolf8904\cmsdbwidgets\forms\CarouselForm;
use werewolf8904\cmsdbwidgets\models\backend\Carousel;


/**
 * Class CarouselService
 *
 */
class CarouselService extends BaseService
{
    public $model_class = CarouselForm::class;
    public $main_model = 'modelCarousel';
    public $main_model_class = Carousel::class;
    public $relations = [
        'modelsItem' => 'items'
    ];
}
