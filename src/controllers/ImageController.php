<?php

namespace werewolf8904\cmsdbwidgets\controllers;


use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmsdbwidgets\models\backend\Image;
use werewolf8904\cmsdbwidgets\models\backend\search\ImageSearch;


/**
 * WidgetImageController implements the CRUD actions for WidgetImage model.
 */
class ImageController extends BackendController
{

    public $searchClass = ImageSearch::class;
    public $class = Image::class;
}
