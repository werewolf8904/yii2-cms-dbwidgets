<?php

namespace werewolf8904\cmsdbwidgets\controllers;

use werewolf8904\cmscore\models\Language;
use werewolf8904\cmsdbwidgets\models\backend\CarouselItem;
use werewolf8904\cmsdbwidgets\models\base\Carousel;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;


/**
 * WidgetCarouselItemController implements the CRUD actions for WidgetCarouselItem model.
 */
class CarouselItemController extends Controller
{

    /**
     * @return string
     */
    public function getViewPath()
    {
        return $this->module->getViewPath() . DIRECTORY_SEPARATOR . 'carousel/item';
    }


    /**
     * Creates a new WidgetCarouselItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param $carousel_id
     *
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidArgumentException
     * @throws HttpException
     */
    public function actionCreate($carousel_id)
    {
        $model = new CarouselItem();
        $carousel = Carousel::findOne($carousel_id);
        if (!$carousel) {
            throw new HttpException(400);
        }

        $model->carousel_id = $carousel->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('alert', ['options' => ['class' => 'alert-success'], 'body' => Yii::t('dbwidgets', 'Carousel slide was successfully saved')]);
            return $this->redirect(['carousel/update', 'id' => $model->carousel_id]);
        }
        $view_params = compact('model', 'carousel');
        $view_params['languages'] = Language::find()->andWhere(['status'=>1])->all();
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', $view_params);
        }
        return $this->render('create', $view_params);
    }

    /**
     * Updates an existing WidgetCarouselItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('alert', ['options' => ['class' => 'alert-success'], 'body' => Yii::t('dbwidgets', 'Carousel slide was successfully saved')]);
            if (!Yii::$app->request->isAjax) {
                return $this->redirect(['carousel/update', 'id' => $model->carousel_id]);
            }

        }
        $view_params = compact('model');
        $view_params['languages'] = Language::find()->andWhere(['status'=>1])->all();
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', $view_params);
        }
        return $this->render('update', $view_params);
    }

    /**
     * Finds the WidgetCarouselItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return CarouselItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarouselItem::find()->where(['id' => $id])->with(['translations'])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Deletes an existing WidgetCarouselItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model && $model->delete()) {
            return $this->redirect(['carousel/update', 'id' => $model->carousel_id,]);
        }

        throw new BadRequestHttpException();
    }
}
