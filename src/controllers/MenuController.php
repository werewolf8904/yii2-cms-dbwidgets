<?php

namespace werewolf8904\cmsdbwidgets\controllers;


use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmsdbwidgets\models\backend\search\MenuSearch;
use werewolf8904\cmsdbwidgets\models\base\Menu;
use werewolf8904\cmsdbwidgets\services\MenuService;


/**
 * WidgetCarouselController implements the CRUD actions for WidgetCarousel model.
 */
class MenuController extends BackendController
{
    public $searchClass = MenuSearch::class;
    public $compositeService = MenuService::class;
    public $class = Menu::class;

}
