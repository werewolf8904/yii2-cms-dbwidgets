<?php

namespace werewolf8904\cmsdbwidgets\controllers;


use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmsdbwidgets\models\backend\Carousel;
use werewolf8904\cmsdbwidgets\models\backend\search\CarouselSearch;
use werewolf8904\cmsdbwidgets\services\CarouselService;


/**
 * WidgetCarouselController implements the CRUD actions for WidgetCarousel model.
 */
class CarouselController extends BackendController
{
    public $searchClass = CarouselSearch::class;
    public $compositeService = CarouselService::class;
    public $class = Carousel::class;

}
