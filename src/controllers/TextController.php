<?php

namespace werewolf8904\cmsdbwidgets\controllers;


use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmsdbwidgets\models\backend\search\TextSearch;
use werewolf8904\cmsdbwidgets\models\backend\Text;


/**
 * WidgetTextController implements the CRUD actions for WidgetText model.
 */
class TextController extends BackendController
{
    public $searchClass = TextSearch::class;
    public $class = Text::class;
}
