<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\cmsdbwidgets\models\base\query\CarouselQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "widget_carousel".
 *
 * @property integer $id
 * @property string $key
 * @property integer $status
 *
 * @property CarouselItem[] $items
 */
class Carousel extends ActiveRecord
{
    public const STATUS_DRAFT = 0;
    public const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_carousel}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    function ($model) {
                        return Carousel::class . $model->key;
                    },
                    self::class,
                ],
            ],
        ];
    }

    /**
     * @return CarouselQuery
     */
    public static function find()
    {
        return new CarouselQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key',], 'required',],
            [['key',], 'unique',],
            [['status',], 'integer',],
            [['status',], 'default', 'value' => 0,],
            [['key',], 'string', 'max' => 1024,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'key' => Yii::t('cmsdbwidgets/model_labels', 'Key'),
            'status' => Yii::t('cmsdbwidgets/model_labels', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(CarouselItem::class, ['carousel_id' => 'id',]);
    }
}
