<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\models\Language;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%widget_carousel_item_i18n}}".
 *
 * @property integer $id
 * @property string $language_code
 * @property integer $widget_carousel_item_id
 * @property string $title
 * @property string $image
 * @property array $image_media
 * @property string $content
 * @property string $alt
 * @property string $url
 *
 * @property Language $languageCode
 * @property CarouselItem $widgetCarouselItem
 */
class CarouselItemI18n extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_carousel_item_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_code', 'widget_carousel_item_id',], 'required',],
            [['content',], 'string',],
            [['title', 'alt',], 'string', 'max' => 255,],
            [['widget_carousel_item_id',], 'integer',],
            [['image', 'url',], 'string', 'max' => 1024,],
            [['title', 'alt', 'image', 'url',], 'default', 'value' => '',],
            [['language_code',], 'string', 'max' => 6,],
            [['image_media',], 'safe',],
            [['image_media',], function ($attribute, $params) {
                if (is_array($this->image_media)) {
                    $this->image_media = array_values($this->image_media);
                }
            },],
            [['widget_carousel_item_id', 'language_code',], 'unique', 'targetAttribute' => ['widget_carousel_item_id', 'language_code',],],
            [['language_code',], 'exist', 'skipOnEmpty' => false, 'targetClass' => Language::class, 'targetAttribute' => ['language_code' => 'code',],],
            [['widget_carousel_item_id',], 'exist', 'skipOnEmpty' => false, 'targetClass' => CarouselItem::class, 'targetAttribute' => ['widget_carousel_item_id' => 'id',],],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'language_code' => Yii::t('cmsdbwidgets/model_labels', 'Language Code'),
            'widget_carousel_item_id' => Yii::t('cmsdbwidgets/model_labels', 'Widget Text ID'),
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Title'),
            'image' => Yii::t('cmsdbwidgets/model_labels', 'Image'),
            'image_media' => Yii::t('cmsdbwidgets/model_labels', 'Image media'),
            'content' => Yii::t('cmsdbwidgets/model_labels', 'Content'),
            'alt' => Yii::t('cmsdbwidgets/model_labels', 'Alt'),
            'url' => Yii::t('cmsdbwidgets/model_labels', 'Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageCode()
    {
        return $this->hasOne(Language::class, ['code' => 'language_code',]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetCarouselItem()
    {
        return $this->hasOne(CarouselItem::class, ['id' => 'widget_carousel_item_id',]);
    }
}
