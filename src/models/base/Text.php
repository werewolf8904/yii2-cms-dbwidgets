<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\cmsdbwidgets\models\base\query\TextQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%widget_text}}".
 *
 * @property integer $id
 * @property string $key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TextI18n[] $widgetTextI18ns
 */
class Text extends ActiveRecord
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DRAFT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_text}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    function ($model) {
                        return Text::class . $model->key;
                    },
                    self::class,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new TextQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key',], 'required',],
            [['key',], 'unique',],
            [['status',], 'integer',],
            [['key',], 'string', 'max' => 1024,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'key' => Yii::t('cmsdbwidgets/model_labels', 'Key'),
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Title'),
            'status' => Yii::t('cmsdbwidgets/model_labels', 'Status'),
            'name' => Yii::t('cmsdbwidgets/model_labels', 'Name'),
            'content' => Yii::t('cmsdbwidgets/model_labels', 'Content'),
        ];
    }

    /**
     * @param null $language_code
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTextI18n($language_code = null)
    {
        $language_code = $language_code ?: Yii::$app->language;
        $query = $this->getTextI18ns()->andWhere([TextI18n::tableName() . '.language_code' => $language_code,]);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextI18ns()
    {
        return $this->hasMany(TextI18n::class, ['widget_text_id' => 'id',]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetTextI18ns()
    {
        return $this->hasMany(TextI18n::class, ['widget_text_id' => 'id',])->indexBy('language_code');
    }
}
