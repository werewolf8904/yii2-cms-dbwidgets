<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\cmsdbwidgets\models\base\query\MenuQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "widget_carousel".
 *
 */
class Menu extends ActiveRecord
{
    public const STATUS_DRAFT = 0;
    public const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_menu}}';
    }

    /**
     * @return \werewolf8904\cmsdbwidgets\models\base\query\MenuQuery
     */
    public static function find()
    {
        return new MenuQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    function () {
                        return self::class;
                    },
                    self::class,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key',], 'required',],
            [['key',], 'unique',],
            [['status',], 'integer',],
            [['status',], 'default', 'value' => 0,],
            [['key',], 'string', 'max' => 255,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'key' => Yii::t('cmsdbwidgets/model_labels', 'Key'),
            'status' => Yii::t('cmsdbwidgets/model_labels', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(MenuItem::class, ['menu_id' => 'id',])->orderBy(['sort' => SORT_DESC,]);
    }
}
