<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\models\Language;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%widget_text_i18n}}".
 *
 * @property integer $id
 * @property string $language_code
 * @property integer $widget_text_id
 * @property string $name
 * @property string $content
 *
 * @property Language $languageCode
 * @property Text $widgetText
 */
class TextI18n extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_text_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_code', 'widget_text_id',], 'required',],
            [['widget_text_id',], 'integer',],
            [['content',], 'string',],
            [['language_code',], 'string', 'max' => 6,],
            [['name',], 'string', 'max' => 255,],
            [['widget_text_id', 'language_code',], 'unique', 'targetAttribute' => ['widget_text_id', 'language_code',],],
            [['language_code',], 'exist', 'skipOnEmpty' => false, 'targetClass' => Language::class, 'targetAttribute' => ['language_code' => 'code',],],
            [['widget_text_id',], 'exist', 'skipOnEmpty' => false, 'targetClass' => Text::class, 'targetAttribute' => ['widget_text_id' => 'id',],]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'language_code' => Yii::t('cmsdbwidgets/model_labels', 'Language Code'),
            'widget_text_id' => Yii::t('cmsdbwidgets/model_labels', 'Widget Text ID'),
            'name' => Yii::t('cmsdbwidgets/model_labels', 'Name'),
            'content' => Yii::t('cmsdbwidgets/model_labels', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageCode()
    {
        return $this->hasOne(Language::class, ['code' => 'language_code',]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetText()
    {
        return $this->hasOne(Text::class, ['id' => 'widget_text_id',]);
    }
}
