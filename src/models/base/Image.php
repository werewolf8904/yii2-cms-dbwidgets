<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\cmsdbwidgets\models\base\query\ImageQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "widget_image".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $key [varchar(255)]
 *
 */
class Image extends ActiveRecord
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DRAFT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_image}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new ImageQuery(static::class);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    function ($model) {
                        return Image::class . $model->key;
                    },
                    self::class,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key',], 'required',],
            [['id', 'status',], 'integer',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'status' => Yii::t('cmsdbwidgets/model_labels', 'Status'),
            'image' => Yii::t('cmsdbwidgets/model_labels', 'Image'),
            'image_media' => Yii::t('cmsdbwidgets/model_labels', 'Image Media'),
            'key' => Yii::t('cmsdbwidgets/model_labels', 'Key'),
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Title'),
            'alt' => Yii::t('cmsdbwidgets/model_labels', 'Alt'),
            'url' => Yii::t('cmsdbwidgets/model_labels', 'Url'),
        ];
    }

    /**
     * @param null $language_code
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImageI18n($language_code = null)
    {
        $language_code = $language_code ?: Yii::$app->language;
        $query = $this->getImageI18ns()->andWhere([ImageI18n::tableName() . '.language_code' => $language_code,]);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImageI18ns()
    {
        return $this->hasMany(ImageI18n::class, ['widget_image_id' => 'id',]);
    }
}
