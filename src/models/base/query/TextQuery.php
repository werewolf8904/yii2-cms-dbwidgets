<?php

namespace werewolf8904\cmsdbwidgets\models\base\query;


use werewolf8904\cmscore\traits\WithCurrentTranslation;
use yii\db\ActiveQuery;

class TextQuery extends ActiveQuery
{
    use WithCurrentTranslation;

    protected $_relation = 'textI18n';

    public function published()
    {
        return $this->andWhere(['status' => 1]);
    }


}