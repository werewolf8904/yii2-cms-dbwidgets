<?php

namespace werewolf8904\cmsdbwidgets\models\base\query;


use yii\db\ActiveQuery;

class MenuQuery extends ActiveQuery
{

    public function published()
    {
        return $this->andWhere(['status' => 1]);
    }
}