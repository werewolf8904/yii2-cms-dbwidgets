<?php

namespace werewolf8904\cmsdbwidgets\models\base\query;


use yii\db\ActiveQuery;

class CarouselQuery extends ActiveQuery
{
    public function published()
    {
        return $this->andWhere([$this->modelClass::tableName() . '.status' => 1]);
    }
}