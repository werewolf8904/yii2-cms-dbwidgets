<?php

namespace werewolf8904\cmsdbwidgets\models\base\query;


use werewolf8904\cmscore\traits\WithCurrentTranslation;
use yii\db\ActiveQuery;

class CarouselItemQuery extends ActiveQuery
{

    use WithCurrentTranslation;

    protected $_relation = 'carouselItemI18n';

    public function published()
    {
        return $this->andWhere([$this->modelClass::tableName() . '.status' => 1]);
    }


}