<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\cmsdbwidgets\models\base\query\CarouselItemQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "widget_carousel_item".
 *
 * @property integer $id
 * @property integer $carousel_id
 * @property integer $status
 * @property integer $sort
 * @property integer $order
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Carousel $carousel
 */
class CarouselItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_carousel_item}}';
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $key = array_search('carousel_id', $scenarios[self::SCENARIO_DEFAULT], true);
        $scenarios[self::SCENARIO_DEFAULT][$key] = '!carousel_id';
        return $scenarios;
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    function ($model) {
                        return Carousel::class . $model->carousel->key;
                    },
                    self::class,
                ],
            ],
        ];
    }

    /**
     * @return CarouselItemQuery
     */
    public static function find()
    {
        return new CarouselItemQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carousel_id',], 'required',],
            [['carousel_id', 'status', 'sort',], 'integer',],
            [['status', 'sort',], 'default', 'value' => 0,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'carousel_id' => Yii::t('cmsdbwidgets/model_labels', 'Carousel ID'),
            'image' => Yii::t('cmsdbwidgets/model_labels', 'Image'),
            'image_media' => Yii::t('cmsdbwidgets/model_labels', 'Image Media'),
            'status' => Yii::t('cmsdbwidgets/model_labels', 'Status'),
            'sort' => Yii::t('cmsdbwidgets/model_labels', 'Sort'),
            'content' => Yii::t('cmsdbwidgets/model_labels', 'Content'),
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Title'),
            'alt' => Yii::t('cmsdbwidgets/model_labels', 'Alt'),
            'url' => Yii::t('cmsdbwidgets/model_labels', 'Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarousel()
    {
        return $this->hasOne(Carousel::class, ['id' => 'carousel_id',]);
    }

    /**
     * @param null $language_code
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarouselItemI18n($language_code = null)
    {
        $language_code = $language_code ?: Yii::$app->language;
        $query = $this->getCarouselItemI18ns()->andWhere([CarouselItemI18n::tableName() . '.language_code' => $language_code,]);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarouselItemI18ns()
    {
        return $this->hasMany(CarouselItemI18n::class, ['widget_carousel_item_id' => 'id',]);
    }
}
