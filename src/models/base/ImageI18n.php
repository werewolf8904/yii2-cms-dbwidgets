<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\models\Language;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%widget_image_i18n}}".
 *
 * @property integer $id
 * @property string $language_code
 * @property integer $widget_image_id
 * @property string $image
 * @property array $image_media
 * @property string $url
 * @property string $alt
 * @property string $title
 *
 * @property Language $languageCode
 * @property Image $widgetImage
 */
class ImageI18n extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_image_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_code', 'widget_image_id',], 'required',],
            [['widget_image_id',], 'integer',],
            [['image',], 'string', 'max' => 1024,],
            [['image_media',], 'safe',],
            [['image_media',], function ($attribute, $params) {
                if (is_array($this->image_media)) {
                    $this->image_media = array_values($this->image_media);
                }
            },],
            [['url', 'alt', 'title',], 'string', 'max' => 255,],
            [['language_code',], 'string', 'max' => 6,],
            [['widget_image_id', 'language_code',], 'unique', 'targetAttribute' => ['widget_image_id', 'language_code',],],
            [['language_code',], 'exist', 'skipOnEmpty' => false, 'targetClass' => Language::class, 'targetAttribute' => ['language_code' => 'code',],],
            [['widget_image_id',], 'exist', 'skipOnEmpty' => false, 'targetClass' => Image::class, 'targetAttribute' => ['widget_image_id' => 'id',],],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'language_code' => Yii::t('cmsdbwidgets/model_labels', 'Language Code'),
            'widget_image_id' => Yii::t('cmsdbwidgets/model_labels', 'Widget Image ID'),
            'image' => Yii::t('cmsdbwidgets/model_labels', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageCode()
    {
        return $this->hasOne(Language::class, ['code' => 'language_code',]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetImage()
    {
        return $this->hasOne(Image::class, ['id' => 'widget_image_id',]);
    }
}
