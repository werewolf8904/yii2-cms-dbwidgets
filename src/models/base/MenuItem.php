<?php

namespace werewolf8904\cmsdbwidgets\models\base;

use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\composite\ILoadSortable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;

/**
 * This is the model class for table "widget_menu_item".
 *
 */
class MenuItem extends ActiveRecord implements ILoadSortable
{
    /**
     * @var
     */
    public $dynAttr;

    /**
     * @var
     */
    protected $_url_data;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_menu_item}}';
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->data = json_decode($this->data, true);
        $this->urlData = base64_encode(json_encode(['data' => ArrayHelper::getValue($this->data, 'url'),
            'class' => $this->menu_class]));
        $this->dynAttr = $this->data['data'];
    }

    /**
     * @inheritdoc
     */
    public function getAttribute($value, $safeOnly = true)
    {
        try {
            return $this->getDynamicAttribute($value);
        } catch (\Exception $e) {
            return parent::getAttribute($value, $safeOnly);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        //$this->data = $this->urlData;
        $bs = parent::beforeSave($insert);
        $data = [];
        if ($bs) {
            if ($this->urlData) {
                $encoded = json_decode(base64_decode($this->urlData));
                $this->menu_class = ArrayHelper::getValue($encoded, 'class');
                $data['url'] = ArrayHelper::getValue($encoded, 'data');
            } else {
                $this->menu_class = \werewolf8904\cmsdbwidgets\links\CustomMenuItem::class;
            }
            $data['data'] = $this->dynAttr;
            $this->data = json_encode($data);
        }
        return $bs;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [
                    function ($model) {
                        return
                            Menu::class . $model->menu->key;
                    },
                ],
            ],
            'dynamicAttribute' => [
                'class' => DynamicAttributeBehavior::class,
                'storageAttribute' => 'dynAttr',
                'dynamicAttributeDefaults' => [
                ],
                'allowRandomDynamicAttribute' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['menu_id',], 'required',],
            [['status',], 'filter', 'filter' => 'boolval',],
            [['status',], 'boolean',],
            [['menu_id', 'sort',], 'integer',],
            [['status', 'sort',], 'default', 'value' => 0,],
            [['menu_class',], 'string',],
            [['data', 'urlData',], 'safe',],
        ];
        foreach (Yii::$container->get(\werewolf8904\cmscore\language\ILanguage::class)->getLanguages() as $key => $value) {
            $rules[] = [
                [
                    'name_' . $key, 'url_' . $key,
                ],
                'string',
            ];
        }
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cmsdbwidgets/model_labels', 'ID'),
            'carousel_id' => Yii::t('cmsdbwidgets/model_labels', 'Carousel ID'),
            'image' => Yii::t('cmsdbwidgets/model_labels', 'Image'),
            'status' => Yii::t('cmsdbwidgets/model_labels', 'Status'),
            'order' => Yii::t('cmsdbwidgets/model_labels', 'Sort'),
            'content' => Yii::t('cmsdbwidgets/model_labels', 'Content'),
            'title' => Yii::t('cmsdbwidgets/model_labels', 'Title'),
            'alt' => Yii::t('cmsdbwidgets/model_labels', 'Alt'),
            'field_1' => Yii::t('cmsdbwidgets/model_labels', 'Field 1'),
            'field_2' => Yii::t('cmsdbwidgets/model_labels', 'Field 2'),
            'field_3' => Yii::t('cmsdbwidgets/model_labels', 'Field 3'),
            'url' => Yii::t('cmsdbwidgets/model_labels', 'Url'),
        ];
    }

    /**
     * @return int
     */
    public function getSortField()
    {
        return $this->sort;
    }

    /**
     * @param $val
     */
    public function setSortField($val)
    {
        $this->sort = $val;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::class, ['id' => 'menu_id',]);
    }

    /**
     * @return mixed
     */
    public function getUrlData()
    {
        return $this->_url_data;
    }

    /**
     * @param mixed $url_data
     */
    public function setUrlData($url_data): void
    {
        if ($url_data !== null) {
            $this->_url_data = $url_data;
        }
    }
}
