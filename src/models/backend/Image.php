<?php

namespace werewolf8904\cmsdbwidgets\models\backend;

use werewolf8904\cmscore\behaviors\MultilingualBehavior;
use werewolf8904\cmscore\models\Language;
use werewolf8904\cmscore\traits\BackendTrait;
use werewolf8904\cmsdbwidgets\models\base\ImageI18n;

/**
 * Class WidgetImage
 */
class Image extends \werewolf8904\cmsdbwidgets\models\base\Image
{
    use BackendTrait;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['ml'] = [
            'class' => MultilingualBehavior::class,
            'languages' => Language::getLanguages(),
            'languageField' => 'language_code',
            //'localizedPrefix' => '',
            'requireTranslations' => false,
            //'dynamicLangClass' => true,
            'langClassName' => ImageI18n::class,
            'defaultLanguage' => \Yii::$app->language,
            'langForeignKey' => 'widget_image_id',
            'tableName' => ImageI18n::tableName(),
            'attributes' => ['image', 'alt', 'title', 'url',  'image_media',]
        ];
        return $behaviors;
    }
}
