<?php

namespace werewolf8904\cmsdbwidgets\models\backend;

use werewolf8904\cmscore\behaviors\MultilingualBehavior;
use werewolf8904\cmscore\models\Language;
use werewolf8904\cmscore\traits\BackendTrait;
use werewolf8904\cmsdbwidgets\models\base\CarouselItemI18n;
use werewolf8904\composite\ILoadSortable;
use Yii;

/**
 * Class WidgetCarouselItem
 *
 * @property int $sortField
 */
class CarouselItem extends \werewolf8904\cmsdbwidgets\models\base\CarouselItem implements ILoadSortable
{
    use BackendTrait;

    /**
     * @return int
     */
    public function getSortField()
    {
        return $this->sort;
    }

    /**
     * @param $val
     */
    public function setSortField($val)
    {
        $this->sort = $val;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['ml'] = [
            'class' => MultilingualBehavior::class,
            'languages' => Language::getLanguages(),
            'languageField' => 'language_code',
            //'localizedPrefix' => '',
            'requireTranslations' => false,
            //'dynamicLangClass' => true,
            'langClassName' => CarouselItemI18n::class,
            'defaultLanguage' => Yii::$app->language,
            'langForeignKey' => 'widget_carousel_item_id',
            'tableName' => '{{%widget_carousel_item_i18n}}',
            'attributes' => ['content', 'url', 'title', 'alt', 'url', 'image', 'image_media',],
        ];
        return $behaviors;
    }
}
