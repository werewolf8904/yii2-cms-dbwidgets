<?php

namespace werewolf8904\cmsdbwidgets\models\backend;


/**
 * Class WidgetCarousel
 *
 */
class Carousel extends \werewolf8904\cmsdbwidgets\models\base\Carousel
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(CarouselItem::class, ['carousel_id' => 'id'])
            ->orderBy(['sort' => SORT_DESC]);
    }
}
