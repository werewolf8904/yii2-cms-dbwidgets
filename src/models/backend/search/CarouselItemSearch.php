<?php

namespace werewolf8904\cmsdbwidgets\models\backend\search;


use werewolf8904\cmsdbwidgets\models\backend\CarouselItem;
use yii\data\ActiveDataProvider;


/**
 * Class WidgetCarouselItemSearch
 *
 */
class CarouselItemSearch extends CarouselItem
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'carousel_id', 'status', 'order'], 'integer'],
            [['path', 'url', 'caption'], 'safe']
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = null)
    {
        $query = parent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (isset($_GET['WidgetCarouselItemSearch']) && !($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'carousel_id' => $this->carousel_id,
            'status' => $this->status,
            'order' => $this->order
        ]);

        $query->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'caption', $this->caption]);

        return $dataProvider;
    }
}
