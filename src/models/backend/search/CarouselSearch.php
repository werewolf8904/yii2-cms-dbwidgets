<?php

namespace werewolf8904\cmsdbwidgets\models\backend\search;


use werewolf8904\cmsdbwidgets\models\backend\Carousel;
use yii\data\ActiveDataProvider;

/**
 * WidgetCarouselSearch
 */
class CarouselSearch extends Carousel
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'status'], 'integer'],
            [['key'], 'safe']
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Carousel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (isset($_GET['WidgetCarouselSearch']) && !($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status
        ]);

        $query->andFilterWhere(['like', 'key', $this->key]);

        return $dataProvider;
    }
}
