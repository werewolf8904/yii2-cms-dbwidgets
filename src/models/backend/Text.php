<?php

namespace werewolf8904\cmsdbwidgets\models\backend;


use werewolf8904\cmscore\behaviors\MultilingualBehavior;
use werewolf8904\cmscore\models\Language;
use werewolf8904\cmscore\traits\BackendTrait;
use werewolf8904\cmsdbwidgets\models\base\TextI18n;


/**
 * Class WidgetText
 *
 */
class Text extends \werewolf8904\cmsdbwidgets\models\base\Text
{
    use BackendTrait;

    /**
     * @return array
     */
    public function behaviors()
    {

        $b = parent::behaviors();
        $b['ml'] = [
            'class' => MultilingualBehavior::class,
            'languages' => Language::getLanguages(),
            'languageField' => 'language_code',
            //'localizedPrefix' => '',
            'requireTranslations' => false,
            //'dynamicLangClass' => true,
            'langClassName' => TextI18n::class,
            'defaultLanguage' => \Yii::$app->language,
            'langForeignKey' => 'widget_text_id',
            'tableName' => TextI18n::tableName(),
            'attributes' => [
                'name', 'content'
            ]
        ];

        return $b;
    }
}