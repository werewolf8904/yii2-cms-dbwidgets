<?php

namespace werewolf8904\cmsdbwidgets\models\frontend;

use werewolf8904\cmsdbwidgets\traits\FrontendImageTrait;

/**
 * Class WidgetImage
 *
 */
class Image extends \werewolf8904\cmsdbwidgets\models\base\Image
{
    use FrontendImageTrait;

    /**
     * @return $this|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return parent::find()->joinWithCurrentTranslation();
    }
}
