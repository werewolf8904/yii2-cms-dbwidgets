<?php

namespace werewolf8904\cmsdbwidgets\models\frontend;

use werewolf8904\cmsdbwidgets\traits\FrontendImageTrait;

/**
 * This is the model class for table "widget_carousel_item".
 */
class CarouselItem extends \werewolf8904\cmsdbwidgets\models\base\CarouselItem
{
    use FrontendImageTrait;

    /**
     * @var
     */
    public $content;

    /**
     * @return \werewolf8904\cmsdbwidgets\models\base\query\CarouselItemQuery
     */
    public static function find()
    {
        return parent::find()->joinWithCurrentTranslation();
    }
}
