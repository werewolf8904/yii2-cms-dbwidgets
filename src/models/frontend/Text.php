<?php

namespace werewolf8904\cmsdbwidgets\models\frontend;


/**
 * Class WidgetText
 *
 */
class Text extends \werewolf8904\cmsdbwidgets\models\base\Text
{

    public $name;
    public $content;

    /**
     * @return $this|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return parent::find()->joinWithCurrentTranslation();
    }

}
