<?php

namespace werewolf8904\cmsdbwidgets\models\frontend;


/**
 * Class Carousel
 *
 * @inheritdoc
 */
class Carousel extends \werewolf8904\cmsdbwidgets\models\base\Carousel
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(CarouselItem::class, ['carousel_id' => 'id']);
    }

}